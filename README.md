This code already added log file to ./data folder.
It just need to go through step 2 to step 5.

Instructions:

    1.  Add log file to ./data folder
    2.  Run command “docker-compose build” 
    3.  Run command “docker-compose up”
    4.  Wait for the data to process (The waiting time for my laptop is 5 mins)
    5.  Go to http://localhost:3000/stats and the Json file will be displayed. If data is not ready, the web server will show “Processing Data. Please wait.”

Approach for this assignment:
Since this assignment is handling log data, I believe the right approach is using the ELK solution (Elasticsearch Logstash Kibana). Elasticsearch is a NoSQL database for storing data. Logstash is for processing and parsing the log file to Elasticsearch. Kibana is a data visualization dashboard software for Elasticsearch. Since I did not set up ELK before, I spent some time studying how to set it up properly.

What I did in this assignmnent: 
    1.  Used this git repo as my base to solve this assignment: (https://github.com/deviantony/docker-elk) For details, please refer to README_ELK.md
    2.  Updated the docker-compose.yml to make the ./data folder able to pass the log file to the logstash docker.
    3.  Modified the logstash.conf to make it able to parse the log data to elasticsearch.
    4.  Added a Flask web to docker-compose.yml to make it able to query the electisearch and return the stats json

Here are some useful information:

User Account and Password (Elasticsearch and Kibana)
    User:       elastic
    Password:   changeme

Ports:
    Kibana:            http://localhost:5601
    Elasticsearch:     http://localhost:9200
    Flask:             http://localhost:3000

Result url: http://localhost:3000/stats

There is another way to read log files from url:
Uncomment the http_poller block in logstash.conf and update the url path (TEST_DATA) from .env file.
