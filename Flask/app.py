import time
from flask import Flask
import requests
from requests.auth import HTTPBasicAuth
import json
from flask_caching import Cache
config = {
    "DEBUG": True,          # some Flask specific configs
    "CACHE_TYPE": "SimpleCache",  # Flask-Caching related configs
    "CACHE_DEFAULT_TIMEOUT": 300
}


app = Flask(__name__)
# tell Flask to use the above defined config
app.config.from_mapping(config)
cache = Cache(app)
cache.init_app(app)

@app.route('/stats', methods=['GET'])
@cache.cached(timeout=50)
def getData():
    try:
        url = 'http://host.docker.internal:9200/_sql?format=json'
        myobj = {'query': 'SELECT url, COUNT(*) FROM test_data Group By url Order By COUNT(*) DESC LIMIT 5'}
        user = 'elastic'
        password = 'changeme'
        headers = {'Content-type': 'application/json'}


        sql_query1 = 'SELECT COUNT ( DISTINCT client ) From test_data'
        sql_query2 = 'SELECT client,COUNT (* ) From test_data GROUP BY client'
        sql_query3 = "SELECT COUNT (* ),method From test_data GROUP BY method"
        sql_query4 = "SELECT url, COUNT(*) FROM test_data Group By url Order By COUNT(*) DESC LIMIT 5"

        data1 = getdata(sql_query1)
        data2 = getdata(sql_query2)
        data3 = getdata(sql_query3)
        data4 = getdata(sql_query4)
        
        data = {
            "Q1": data1,
            "Q2": data2,
            "Q3": data3,
            "Q4": data4
            }
            
        return json.dumps(data)
    except:    
        return "Processing Data. Please wait."

def getdata(sql_query):
    url = 'http://host.docker.internal:9200/_sql?format=json'
    myobj = {'query': sql_query}
    user = 'elastic'
    password = 'changeme'
    headers = {'Content-type': 'application/json'}
    data = requests.post(url, json= myobj, auth = HTTPBasicAuth(user,password))
    dataObj = json.loads(data.text)
    arr = []
    while 'cursor' in dataObj:
        arr.extend(dataObj['rows'])
        data= getJson( dataObj['cursor'] ) 
        dataObj = json.loads(data.text)
    arr.extend(dataObj['rows'])   
    return json.dumps(arr)



def getJson(cursor):
    url = 'http://host.docker.internal:9200/_sql?format=json'
    user = 'elastic'
    password = 'changeme'
    headers = {'Content-type': 'application/json'}
    myobj = {'cursor': cursor}
    data = requests.post(url, json= myobj, auth = HTTPBasicAuth(user,password))
    return data