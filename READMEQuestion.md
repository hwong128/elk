# SRE Take Home Assessment

**NOTE**: This assessment is intended to be completed in 4 hours or less. Please do not use any additional time, even to attempt the bonus goals. The bonus challenge is not a requirement for your submission to be accepted.

**Submitting the assignment**: Either host a public github/gitlab repo or zip up the files necessary and send back to the recruiter.

**Assignment:**

We would like to pull some aggregated stats about our access log files (see attached file for an example) via an HTTP request. We are interested in the following information:


- A count of unique IP addresses
- A map of IP address to number of requests (how many requests did each IP make?)
- The distribution of HTTP status codes returned
- The top 5 referrers for GET requests

In order to serve up this information, create a docker container that takes an access log file on the host filesystem as an argument, and returns the above information in JSON format on an HTTP endpoint `/stats`. Any subsequent calls to `/stats` should return an updated set, assuming the file changes.

Consider this to be a tool that you will pass on to other engineers or stakeholders, and be sure to include any documentation or instructions on how to get it running.

**Bonus:**

Assume that the log file you are pulling stats from grows at 10000 lines per second, and is rotated every hour. Ensure that the above service will remain performant and accurate in this environment.


